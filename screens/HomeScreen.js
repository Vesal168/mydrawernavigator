import React from 'react';
import {
  StyleSheet,
  Image,
  ScrollView,
  SafeAreaView,
  Text,
  View,
  TouchableHighlight,
  Button,
  TouchableOpacity
} from 'react-native';

import ListPost from '../screens/ListPost';




class HomeScreen extends React.Component {
  onPressBtn = () => {
    alert('Ok');
  };
  render() {
    return (
      <SafeAreaView style={styles.safe_area_view}>
        <ScrollView>
          <View style={styles.header}>
            <Image
              source={{
                uri:
                  'https://download.logo.wine/logo/Facebook/Facebook-Logo.wine.png',
              }}
              style={styles.logofb}
            />
            
          </View>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{width: 80, height: 50}}>
              <TouchableHighlight
                style={[
                  styles.profileImgContainer,
                  {borderColor: 'green', borderWidth: 1},
                ]}>
                <Image
                  source={{
                    uri:
                      'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/121360301_773769253184601_3889317023053852003_n.jpg?_nc_cat=102&_nc_sid=09cbfe&_nc_ohc=sb0Gw7zG8mIAX875dBD&_nc_ht=scontent.fpnh7-1.fna&oh=74d2d4d16c39c4458b5a8b5c3b3d8058&oe=5FB2C700',
                  }}
                  style={styles.profileImg}
                />
              </TouchableHighlight>
            </View>
            <View
              style={{
                width: 50,
                flex: 2,
                height: 50,
              }}>
              <TouchableOpacity
                style={styles.loginScreenButton}
                onPress={this.onPressBtn.bind(this)}
                underlayColor="#fff">
                <Text style={styles.loginText}>What's on your mind ?</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.placeholderstyle}></View>
          <View style={styles.padd_subprofile}>
            <ScrollView horizontal={true}>
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/121360301_773769253184601_3889317023053852003_n.jpg?_nc_cat=102&_nc_sid=09cbfe&_nc_ohc=sb0Gw7zG8mIAX875dBD&_nc_ht=scontent.fpnh7-1.fna&oh=74d2d4d16c39c4458b5a8b5c3b3d8058&oe=5FB2C700',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/70139170_1166387326902074_3402885434521419776_o.jpg?_nc_cat=106&_nc_sid=09cbfe&_nc_ohc=wgjTiCOfhfkAX9BY5Ff&_nc_ht=scontent.fpnh7-1.fna&oh=592831e1c5a54f5fee9d9a0f64996f49&oe=5FAF48ED',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/120040930_2707486469526084_6175162510066141712_o.jpg?_nc_cat=104&_nc_sid=09cbfe&_nc_ohc=Ysv5CjPAYwkAX9n6mp0&_nc_ht=scontent.fpnh7-1.fna&oh=8538c94bae06fe2dd5c7b5dae0b13440&oe=5FB1BF64',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-1/s200x200/118164720_2384612661844427_5395686764093233667_n.jpg?_nc_cat=111&_nc_sid=7206a8&_nc_ohc=RHilTMb1g3kAX9DaibF&_nc_ht=scontent.fpnh7-1.fna&tp=7&oh=f2cf73e949b0ce64dfb5b50b9b6b2b7d&oe=5FB27BC6',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/120150307_3231499510304941_5704195927520683322_o.jpg?_nc_cat=105&_nc_sid=09cbfe&_nc_ohc=DON36T5uAgoAX8wEXHe&_nc_ht=scontent.fpnh7-1.fna&oh=f28d4de0e83999d566af0ba87756302d&oe=5FAF385E',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/120542093_1679677318866676_3532534726043566773_n.jpg?_nc_cat=105&_nc_sid=09cbfe&_nc_ohc=5JXpsXGqINwAX8GwiIT&_nc_ht=scontent.fpnh7-1.fna&oh=c89c56af7dc407477cb80db46b438d13&oe=5FAF5FC2',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/92136177_1573635562789711_3848051196954673152_n.jpg?_nc_cat=100&_nc_sid=09cbfe&_nc_ohc=c05Qlmz-1ZwAX8FAmZJ&_nc_ht=scontent.fpnh7-1.fna&oh=cf1c0f816c51e914cce7c917f7808018&oe=5FB1A09F',
                }}
                style={styles.subprofile}
              />
              <Image
                source={{
                  uri:
                    'https://www.t-nation.com/system/publishing/articles/10005529/original/6-Reasons-You-Should-Never-Open-a-Gym.png',
                }}
                style={styles.subprofile}
              />
            </ScrollView>
          </View>
          {/* ///////// */}
          <View style={styles.placeholderstyle}></View>
          <View style={styles.storiesList}>
            <View style={styles.storiesItem}>
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/120142372_756053578289502_5937728907539760767_o.jpg?_nc_cat=102&_nc_sid=8bfeb9&_nc_ohc=5PeDeMJvNv0AX-wWp1r&_nc_ht=scontent.fpnh7-1.fna&oh=edc75d0c2b5337cfd855ac263260541b&oe=5FB16C01',
                }}
                style={styles.imgStories}
              />
              <View style={{marginTop: -55}}>
                <View
                  style={{
                    position: 'absolute',
                    backgroundColor: 'white',
                    width: 200,
                    height: 200,
                  }}>
                  <Text style={{marginLeft: 36, color: 'blue', marginTop: 10}}>
                    {' '}
                    Create
                  </Text>
                  <Text style={{marginLeft: 15}}>Create a Story</Text>
                </View>
              </View>
            </View>

            <View style={styles.container}>
              <ScrollView horizontal={true}>
                <View style={styles.storiesItem}>
                  <Image
                    source={{
                      uri:
                        'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/46033581_348736812354516_8254295095524720640_n.jpg?_nc_cat=102&_nc_sid=8bfeb9&_nc_ohc=GML5dgc_M9cAX_k_8C6&_nc_ht=scontent.fpnh7-1.fna&oh=f99ded44d37be0ba5e43e2c696cc472d&oe=5FAF8EEC',
                    }}
                    style={styles.imgStories}
                  />
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/6ab38a63-46f8-4210-8776-288a3118d455/da35re7-6ff298e7-663d-4779-84d5-377309551a0b.png/v1/fill/w_783,h_1021,strp/sad_boy_squad_by_killer_instincts_da35re7-pre.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD0xMzM2IiwicGF0aCI6IlwvZlwvNmFiMzhhNjMtNDZmOC00MjEwLTg3NzYtMjg4YTMxMThkNDU1XC9kYTM1cmU3LTZmZjI5OGU3LTY2M2QtNDc3OS04NGQ1LTM3NzMwOTU1MWEwYi5wbmciLCJ3aWR0aCI6Ijw9MTAyNCJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.JeJGwfiyvwCIi2OQvsno6QJtAKldMf7FWAJc0RgsKxE',
                      }}
                    />

                    <View>
                      <Text
                        style={{
                          color: 'white',
                          textAlign: 'center',
                          paddingLeft: 10,
                          paddingRight: 10,
                          padding: 150,
                        }}>
                        Kendall Jenner
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.storiesItem}>
                  <Image
                    source={{
                      uri:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSrkqGtX0vmhYsqZXKk4mC3bm5ZQjgASOEa-g&usqp=CAU',
                    }}
                    style={styles.imgStories}
                  />
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://purepng.com/public/uploads/thumbnail/animated-sad-boy-x1d.png',
                      }}
                    />

                    <View>
                      <Text
                        style={{
                          color: 'white',
                          textAlign: 'center',
                          paddingLeft: 10,
                          paddingRight: 10,
                          padding: 150,
                        }}>
                        Kendall Jenner
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.imgStories}>
                  <Image
                    source={{
                      uri:
                        'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/46144909_348736629021201_1308421212170354688_n.jpg?_nc_cat=111&_nc_sid=8bfeb9&_nc_ohc=kT5nUB44C9MAX8b58S0&_nc_ht=scontent.fpnh7-1.fna&oh=d028dc222d15c6d215d18c9fe607800c&oe=5FB1E3D7',
                    }}
                    style={styles.imgStories}
                  />
                  <View style={{marginTop: -179, marginLeft: 4}}>
                    <Image
                      style={{
                        height: 48,
                        width: 48,
                        borderRadius: 40,
                        position: 'absolute',
                      }}
                      source={{
                        uri:
                          'https://imageproxy.themaven.net//https%3A%2F%2Fwww.biography.com%2F.image%2FMTQyMDA0OTY4OTA2MzAyNTI2%2Fkendall-jenner_gettyimages-477504950jpg.jpg',
                      }}
                    />

                    <View>
                      <Text
                        style={{
                          color: 'white',
                          textAlign: 'center',
                          paddingLeft: 10,
                          paddingRight: 10,
                          padding: 150,
                        }}>
                        Kendall Jenner
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.imgStories}>
                  <Image
                    source={{
                      uri:
                        'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-0/p526x296/121968753_3600046300031903_2593463388784901578_o.jpg?_nc_cat=1&_nc_sid=730e14&_nc_ohc=7U5XStSwLjoAX_ZWKfA&_nc_ht=scontent.fpnh7-1.fna&tp=6&oh=397c3272e1c772e597498c44da501a26&oe=5FB1C151',
                    }}
                    style={styles.imgStories}
                  />
                </View>
                <View style={styles.imgStories}>
                  <Image
                    source={{
                      uri:
                        'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-0/p526x296/121127202_363052165038761_5122724015536999625_n.jpg?_nc_cat=106&_nc_sid=8bfeb9&_nc_ohc=QmvlAJHIr5IAX-luv2M&_nc_ht=scontent.fpnh7-1.fna&tp=6&oh=9940cbf2ab1e3f5daa9b0b8b6079acba&oe=5FB19030',
                    }}
                    style={styles.imgStories}
                  />
                </View>
                <View style={styles.imgStories}>
                  <Image
                    source={{
                      uri:
                        'https://cnet1.cbsistatic.com/img/glzfXa3w2IvUUeT22ifnHCQxaoM=/940x0/2019/06/11/02e36daa-1122-4056-81d0-0a1fd040fdf6/avenger-03.jpg',
                    }}
                    style={styles.imgStories}
                  />
                </View>
              </ScrollView>
            </View>
          </View>
          <View style={styles.placeholderstyle}></View>
          <View>
            <ScrollView>
              <ListPost />
            </ScrollView>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 100,
  },
  storiesList: {
    flex: 1,
    flexDirection: 'row',
    marginRight: 15,
    marginLeft: 15,
  },
  storiesItem: {
    width: 130,
    height: 185,
    backgroundColor: 'powderblue',
    margin: 5,
    borderRadius: 10,
  },
  imgStories: {
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    borderRadius: 10,
  },
  safe_area_view: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    padding: 15,
    marginLeft: -16,
    bottom: 6

  },
  logofb: {
    width: 200,
    height: 40,
  },
  profileImgContainer: {
    marginLeft: 23,
    height: 50,
    width: 50,
    borderRadius: 40,
  },
  profileImg: {
    height: 48,
    width: 48,
    borderRadius: 40,
  },
  subprofile: {
    height: 50,
    width: 50,
    borderRadius: 40,
    margin: 10,
  },
  padd_subprofile: {
    marginRight: 20,
    marginLeft: 15,
  },
  postbtn: {
    backgroundColor: '#dc143c',
  },
  loginScreenButton: {
    marginRight: 10,
    paddingTop: 10,
    paddingBottom: 5,
    backgroundColor: '#f5f5f5',
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#fff',
    width: '95%',
  },
  loginText: {
    color: '#000',
    textAlign: 'left',
    paddingLeft: 20,
    padding: 8,
  },
  placeholderstyle: {
    padding: 6,
    flex: 1,
    backgroundColor: '#ccc',
    flexDirection: 'row',
    marginTop: 8,
  },
});
export default HomeScreen;
