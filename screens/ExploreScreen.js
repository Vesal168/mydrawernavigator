import React from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  Image,
} from 'react-native';
import Swiper from 'react-native-swiper';
import {
  Card,
  CardItem,
  Thumbnail,
  Button,
  Icon,
  Left,
  Body,
  Right,Container
} from 'native-base';
import { ScrollView } from 'react-native';
const DATA = [
  {
    id: '1',
    title: 'Mac Book Pro 2020',
    price: '2000$',
    img: 'https://qa-platforms.com/wp-content/uploads/2020/07/2-7.jpg',
  },
  {
    id: '2',
    title: 'Speaker Harvet',
    price: '6000$',
    img: 'https://qa-platforms.com/wp-content/uploads/2020/07/2-7.jpg',
  },
  {
    id: '3',
    title: 'Third Item',
    price: '5000$',
    img: 'https://qa-platforms.com/wp-content/uploads/2020/07/2-7.jpg',
  },
  {
    id: '4',
    title: 'Speaker Harvet',
    price: '6000$',
    img: 'https://qa-platforms.com/wp-content/uploads/2020/07/2-7.jpg',
  }
];

const Item = ({img, title}) => (
  
  <Card>
    <CardItem cardBody>
      <Image
        source={{
          uri: img,
        }}
        style={{height: 200, width: null, flex: 1}}
      />
    </CardItem>
    <CardItem>
      <Left>
        <Button block success>
          <Text style={{color: 'white', overflow: 'hidden', padding: 12}}>
            Add to Card
          </Text>
        </Button>
      </Left>
      <Body>
        <Right>
          <Button transparent>
            <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: 16, color: '#ccc'}}>{title}</Text>
            </View>
          </Button>
        </Right>
      </Body>
    </CardItem>
  </Card>
);

const ExploreScreen = () => {
  const renderItem = ({item}) => <Item 
  img={item.img}
  title={item.title} 
  />
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.sliderContainer}>
        <Swiper
          autoplay
          horizontal={false}
          height={200}
          activeDotColor="#FF6347">
          <View style={styles.slide}>
            <Image
              source={require('../assets/slide1.webp')}
              resizeMode="cover"
              style={styles.sliderImage}
            />
          </View>
          <View style={styles.slide}>
            <Image
              source={require('../assets/slide2.webp')}
              resizeMode="cover"
              style={styles.sliderImage}
            />
          </View>
          <View style={styles.slide}>
            <Image
              source={require('../assets/slide3.webp')}
              resizeMode="cover"
              style={styles.sliderImage}
            />
          </View>
        </Swiper>
      </View>
      <View style={styles.placeStyle}></View>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  profileImg: {
    height: 48,
    width: 48,
    borderRadius: 40,
  },
  sliderContainer: {
    height: 200,
    width: '100%',
    marginTop: 20,
    justifyContent: 'center',
    alignSelf: 'center',
    bottom: 21,
  },

  wrapper: {},

  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderRadius: 8,
  },
  sliderImage: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
    borderRadius: 8,
  },
  placeStyle:{
    padding: 3,
    flex: 1,
    backgroundColor: '#ccc',
    flexDirection: 'row',
    marginTop: 1,
  }
});

export default ExploreScreen;
